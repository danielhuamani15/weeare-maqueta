# weeare-maqueta

Landing page for Weeare

## Getting started

1.  Install with npm:

```shell
npm install
```

2.  Run the project

```shell
npm run dev
```

3.  Now open http://localhost:1234/ in your browser. If needed, you can also override the default port with the `-p` option. Add `--open` to automatically open a browser.

See [parceljs.org](https://parceljs.org) for more documentation!


## Deployment

1.  Make the dist project to deploy

```shell
npm run build
```

2.  Copy the dist directory



